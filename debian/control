Source: globus-callout
Priority: optional
Maintainer: Mattias Ellert <mattias.ellert@physics.uu.se>
Build-Depends:
 debhelper-compat (= 13),
 dpkg-dev (>= 1.22.5),
 pkgconf,
 libglobus-common-dev (>= 15),
 libltdl-dev
Build-Depends-Indep:
 doxygen
Standards-Version: 4.6.2
Section: net
Vcs-Browser: https://salsa.debian.org/ellert/globus-callout
Vcs-Git: https://salsa.debian.org/ellert/globus-callout.git
Homepage: https://github.com/gridcf/gct/

Package: libglobus-callout0t64
Provides: ${t64:Provides}
Replaces: libglobus-callout0
Breaks: libglobus-callout0 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Description: Grid Community Toolkit - Globus Callout Library
 The Grid Community Toolkit (GCT) is an open source software toolkit used for
 building grid systems and applications. It is a fork of the Globus Toolkit
 originally created by the Globus Alliance. It is supported by the Grid
 Community Forum (GridCF) that provides community-based support for core
 software packages in grid computing.
 .
 The libglobus-callout0t64 package contains:
 Globus Callout Library - provides a platform independent way of
 dealing with runtime loadable functions.

Package: libglobus-callout-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libglobus-callout0t64 (= ${binary:Version}),
 ${misc:Depends},
 libglobus-common-dev (>= 15)
Suggests:
 libglobus-callout-doc (= ${source:Version})
Description: Grid Community Toolkit - Globus Callout Library Development Files
 The Grid Community Toolkit (GCT) is an open source software toolkit used for
 building grid systems and applications. It is a fork of the Globus Toolkit
 originally created by the Globus Alliance. It is supported by the Grid
 Community Forum (GridCF) that provides community-based support for core
 software packages in grid computing.
 .
 The libglobus-callout-dev package contains:
 Globus Callout Library Development Files

Package: libglobus-callout-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends}
Description: Grid Community Toolkit - Globus Callout Library Documentation Files
 The Grid Community Toolkit (GCT) is an open source software toolkit used for
 building grid systems and applications. It is a fork of the Globus Toolkit
 originally created by the Globus Alliance. It is supported by the Grid
 Community Forum (GridCF) that provides community-based support for core
 software packages in grid computing.
 .
 The libglobus-callout-doc package contains:
 Globus Callout Library Documentation Files
